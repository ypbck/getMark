<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="./jquery/jquery.mobile-1.4.5.min.css">
<script src="./jquery/jquery.min.js"></script>
<script src="./jquery/jquery.mobile-1.4.5.min.js"></script>

<style type="text/css">
	.ui-content{
		background-color: #FFFFCC;
		color:BLACK;
	}
	#titles{
		//color: #003366;
    //background-color: #99CCCC;
	}
	#doget{
		background-color: #99CCCC;
	}
  #result{
    padding: 10px;
    background-color: #CCFFCC;
  }
  #ttt{
      color: #339933;

  }


</style>

</head>
<body>

<div data-role="page">
  <div data-role="header" id="titles">
  <h3>成绩查询</h3>
  </div>

  <center id="ttt"><h3>大学英语四，六级成绩查询</h3></center>

  <?php
      #header("Content-type: text/html; charset=utf8");
      $name = $_POST['name'];
      $id = $_POST['id'];
      $yzm = $_POST['yzm'];
      
      $yzmname = $_POST['yzmname'];
      $name = urlencode($name);
      $yzm = urlencode($yzm);
      $homeDir = __DIR__;
  ?>

  <div data-role="main" class="ui-content">
    <form method="post" action="cet.php">
      <div class="ui-field-contain">

	      <input type="text" name="id" id="id" placeholder="请输入准考证号..">
	      <input type="text" name="name" id="name" placeholder="请输入姓名..">
          <input type="text" name="yzm" id="yzm" placeholder="请输入下方验证码..">
        <?php
            if($name=="" && $id=="" && $yzm==""){
              $output = shell_exec("python3 $homeDir/py/cet.py get 2 3 4 $homeDir/py/cache/");
              $array = explode(',', $output);
              $res = urldecode($array[0]);
              $res = str_replace("\r|\n","",$res);
              echo "<input type='hidden' name='yzmname' value='$res'>";
              echo "<img src='py/cache/$res.jpg'>";
            }
        ?>
        <a href="#" onclick="window.location.reload()">看不清?  换一张</a>

      </div>
      <center>
      	 <input id="doget" type="submit" data-inline="true" value="查询">
      </center>
    </form>
  </div>

  <div id="result">
  <p id="resres"></p>
    <?php

        if($name!="" && $id!=""){
            $yzmname = trim($yzmname);
            $parm = "python3 $homeDir/py/cet.py $id $name $yzm $yzmname $homeDir/py/cache/";
            $output = shell_exec($parm);
            $array = explode("b'", $output);
            $res = "";
            foreach ($array as $value) {
              $res = $res.urldecode($value);
            }
            #echo "<input type='hidden' id='rrr' value=\"$res\">";
            $res = explode("'",$res);
            for($i=1;$i<sizeof($res);$i++){
               echo "$res[$i]</br>";
            }

        }
    ?>
  </div>

</div>

</body>
</html>
