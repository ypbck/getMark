# getMark

#### 项目介绍
本项目是为新疆教育学院开发的成绩查询集合站点。融合了四六级、MHK，计算机考试，考研等考试的成绩查询（项目还在不断完善中。。。），本项目的意义主要在于将这些琐碎的站点融合起来，为公众号的运营吸粉，同时，也能掌握大家第一手的成绩信息。

#### 软件架构
1. 服务器端用php为用户提供成绩查询界面以及接受用户的查询请求参数，php调用py目录下的对应py脚本进行成绩查询，并将成绩结果返回给php，展示到页面上。
2. 对于有验证码的成绩查询会两次调用py脚本，两次调用的会话由cookie保持，cookie将会在第一次请求时保存在文件中，第二次访问时读取文件
3. php获取py返回的参数时会有乱码，故使用二进制编码的方式加以解决。

# 详细的就不解释了，请自行阅读源码，100行上下


#### 安装教程

##服务器配置：
#安装：
1. 安装python3
2. 安装python的requests模块
3. 安装python的bs4模块
4. 安装baidu-aip 模块
注：python模块安装说明：所有模块安装推荐使用pip安装。若模块安装后依然提示“ImportError: No module named 'xxx'”，则换为使用pip3安装即可。
#配置：
1. 项目getMark/py/cache 的cache文件夹必须用root授予777权限，不然py脚本写文件会失败（网站以root身份在运行的话不用授权）
2. 服务器务必安装中文语言支持（否则自行修改php，python，以及系统的环境变量，加载字符，默认字符为UTF8。但不推荐）
3. 项目py/cache目录是缓存文件,随着查询的增多，会大量增加，为节省空间，加快文件检索速度，建议用shell脚本定期（凌晨）清除，释放空间

#### 贡献者
1. 尹鹏博
2. 孙士标


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
