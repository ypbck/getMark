import requests
from urllib import request
from bs4 import BeautifulSoup
from urllib import parse
import sys
import time

class CET:
    def __init__(self):
        self.s = requests.session()
        self.cookies = '__utmt=1; ' \
         '__utma=65168252.468897637.1517456114.1517456114.1520232128.2; ' \
         '__utmb=65168252.7.10.1520232128; ' \
         '__utmc=65168252; ' \
         '__utmz=65168252.1517456114.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); '

        self.headers = {
            'Host': 'www.chsi.com.cn',
            'Referer': 'http://www.chsi.com.cn/cet/',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0'
        }

    def get_yzm(self):
        t = time.time()
        yzmName = str(int(round(t * 1000)))
        print(yzmName)
        yzm_url = 'http://www.chsi.com.cn/cet/ValidatorIMG.JPG?ID=3677.4286808430857'
        yz = self.s.get(yzm_url, headers=self.headers).content
        with open(path+yzmName+'.jpg', 'wb+') as jpg:
            jpg.write(yz)
        ck = self.s.cookies.get_dict()
        for key, value in zip(ck.keys(), ck.values()):
            self.cookies += key + "=" + value + ';'
        with open(path+yzmName+".txt", 'w') as ck:
            ck.write(self.cookies)

    def get_data(self):
        datas = []
        with open(path+yzmName+".txt", 'r') as ck:
            cookie = ck.read()
        #yzm = input("输入验证码:")
        url_ = 'http://www.chsi.com.cn/cet/query?'
        params = {
            'zkzh': num,
            'xm': name,
            'yzm': yzm
        }
        self.headers['Cookie'] = cookie
        url = url_ + parse.urlencode(params)
        html = requests.get(url, headers=self.headers).text
        '''提取数据'''
        soup = BeautifulSoup(html, 'lxml')
        content = soup.find('div', attrs={'class': 'm_cnt_m'})
        result_item = content.findAll("td")
        for i in result_item:
            datas.append(i.getText())
        print(request.quote(""+datas[0].strip()).encode('utf-8'))
        print(request.quote("学   校:"+datas[1].strip()).encode('utf-8'))
        print(request.quote("考试级别:"+datas[2].strip()).encode('utf-8'))
        print(request.quote("准考证号:"+datas[3].strip()).encode('utf-8'))
        print(request.quote("总   分:"+datas[4].strip()).encode('utf-8'))
        print(request.quote("听   力:"+datas[6].strip()).encode('utf-8'))
        print(request.quote("阅   读:"+datas[8].strip()).encode('utf-8'))
        print(request.quote("写作和翻译:"+datas[10].strip()).encode('utf-8'))
        print(request.quote(""+datas[11].strip()).encode('utf-8'))
        print(request.quote(""+datas[12].replace('\r\n ', '')).encode('utf-8'))

if __name__ == '__main__':
    #name = '夏锴'
    #num = '340080172115023'
    num = sys.argv[1]
    name = sys.argv[2]
    name = request.unquote(name)
    yzm = sys.argv[3]
    yzm = request.unquote(yzm)
    yzmName = sys.argv[4]
    path = sys.argv[5]
    cet = CET()
    if(num=="get"):cet.get_yzm()
    else:cet.get_data()