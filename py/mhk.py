from urllib import request
import requests
from aip import AipOcr
from bs4 import BeautifulSoup
import sys
import time

def get_yzm(file_path):  # 处理验证码
    APP_ID = '10791852'
    API_KEY = 'xK9mmVIQNTHPURfd44DKpzBP'
    SECRET_KEY = 'U4i12gvyPXU76dpZhal9e9jzFMmKffv5'

    # 初始化AipFace对象
    client = AipOcr(APP_ID, API_KEY, SECRET_KEY)
    # 定义参数变量
    options = {
        'detect_direction': 'true',
        'language_type': 'CHN_ENG',
    }
    with open(file_path, 'rb') as fp:
        image = fp.read()
    return client.basicGeneral(image, options)

def get_data(choice):
    try:
        s = requests.Session()
        headers = {
            'Host': 'www.xjedu.gov.cn',
            'Content-Length': '99',
            'Origin': 'http://www.xjedu.gov.cn',
            'Cache-Control': 'max-age=0',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
            'Referer': 'http://www.xjedu.gov.cn/webpub/bpf/cjcx/search.jsp?sea_id=1117' + choice
        }
        t = time.time()
        yzmName = path+"/"+str(int(round(t * 1000)))+"mhk.png"
        yzm_url = 'http://www.xjedu.gov.cn/CheckCode.do'
        with open(yzmName, 'wb+') as png:
            png.write(s.get(yzm_url).content)
        result = get_yzm(yzmName)
        yzm = result['words_result'][0]['words']
        url = 'http://www.xjedu.gov.cn/webpub/bpf/cjcx/search.jsp?sea_id=1117' + choice
        params = {
            'act': 'submit',
            'J': ID,
            'randomCode': yzm,
            'submit': '查　询',
            'commond': 'search'
        }

        html = s.post(url, data=params, headers=headers).text
        soup = BeautifulSoup(html, 'lxml')
        con = soup.findAll('td', attrs={'bgcolor': '#FFFFFF'})
        for i in con:
            data = i.findAll("td", attrs={'bgcolor': '#FFFFFF'})
            for ii in data:
                print(request.quote(ii.getText().strip()).encode('utf-8'))
    except:
        print(request.quote('抱歉没有找到与检索条件相符的信息，请检查您输入的身份证号码是否正确!').encode('utf-8'))

if __name__ == '__main__':
    #ID = '650102199603204038'  # MHK四级
    #choice = "4"
    # ID = '653122199406023172'    # MHK三级
    #ID = input('请输入身份证号: ')
    ID = sys.argv[1]
    choice = sys.argv[2]
    path = sys.argv[3]
    #choice = input('MHK三级成绩查询(3)、 MHK四级成绩查询(4) :')
    if (choice=="3"):
        get_data('0')
    elif (choice=="4"):
        get_data('1')
